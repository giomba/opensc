#include "gcns.hpp"

#include <vector>

using namespace gcns;

PersonalData::PersonalData(const uint8_t* buffer, size_t len) {
    std::vector<std::string> field;

    // TODO check length at the beginning?
    for (int i = 12; i < len;) {
        if (buffer[i] == '\0') break;

        std::string hexstring((const char*)&buffer[i], 2);
        int len = std::stoi(hexstring, nullptr, 16);
        i += 2;
        std::string fieldData((const char*)&buffer[i], len);
        i += len;

        field.push_back(fieldData);
    }

    for (int i = 0; i < (int)field.size(); ++i) {
        switch (i) {
            case 0:
                this->issue_date.year =
                    std::stoi(field[i].substr(4, 4), nullptr);
                this->issue_date.month =
                    std::stoi(field[i].substr(2, 2), nullptr);
                this->issue_date.day =
                    std::stoi(field[i].substr(0, 2), nullptr);
                break;
            case 1:
                this->expiration_date.year =
                    std::stoi(field[i].substr(4, 4), nullptr);
                this->expiration_date.month =
                    std::stoi(field[i].substr(2, 2), nullptr);
                this->expiration_date.day =
                    std::stoi(field[i].substr(0, 2), nullptr);
                break;
            case 2:
                this->family_name = field[i];
                break;
            case 3:
                this->first_name = field[i];
                break;
            case 4:
                this->birth_date.year =
                    std::stoi(field[i].substr(4, 4), nullptr);
                this->birth_date.month =
                    std::stoi(field[i].substr(2, 2), nullptr);
                this->birth_date.day =
                    std::stoi(field[i].substr(0, 2), nullptr);
                break;
            case 5:
                this->gender = field[i] == "F" ? GENDER_FEMALE : GENDER_MALE;
                break;
            case 7:
                this->fiscal_code = field[i];
                break;
            case 9:
                this->birth_place = field[i];
                break;
            case 12:
                this->residence_place = field[i];
                break;
            default:
                break;
        }
    }
}
