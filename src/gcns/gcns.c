/*
 * gcns.c: A reader of Italian healtcare smartcards with libopensc
 *
 * Copyright (C) 2022  Giovan Battista Rolandi <giomba@linux.it>
 * based on previous work by
 * Copyright (C) 2001  Juha Yrjölä <juha.yrjola@iki.fi>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "gcns.h"

#include "libopensc/asn1.h"
#include "tools/util.h"

static int opt_wait = 0;
static const char *opt_reader = NULL;
static sc_context_t *ctx = NULL;
static sc_card_t *card = NULL;
sc_context_param_t ctx_param;

int gcns_init() {
    int r, err = 0;
    int lcycle = SC_CARDCTRL_LIFECYCLE_ADMIN;

    memset(&ctx_param, 0, sizeof(ctx_param));
    ctx_param.ver = 0;

    r = sc_context_create(&ctx, &ctx_param);
    if (r) {
        fprintf(stderr, "Failed to establish context: %s\n", sc_strerror(r));
        return GCNS_INIT;
    }

    ctx->flags |= SC_CTX_FLAG_ENABLE_DEFAULT_DRIVER;

    err = util_connect_card_ex(ctx, &card, opt_reader, opt_wait, 0, 0);
    if (err) {
        return GCNS_INIT;
    }

    r = sc_lock(card);
    if (r == SC_SUCCESS)
        r = sc_card_ctl(card, SC_CARDCTL_LIFECYCLE_SET, &lcycle);
    sc_unlock(card);
    if (r && r != SC_ERROR_NOT_SUPPORTED) {
        fprintf(stderr, "unable to change lifecycle: %s\n", sc_strerror(r));
        return GCNS_INIT;
    }

    return GCNS_SUCCESS;
}

int gcns_close() {
    if (card) {
        sc_disconnect_card(card);
    }
    if (ctx) sc_release_context(ctx);
    return GCNS_SUCCESS;
}

int gcns_read_personal_data(u8 *buffer, size_t len) {
    sc_path_t path;
    int r;

    sc_format_path("3F0011001102", &path);
    r = sc_select_file(card, &path, NULL);
    if (r) {
        fprintf(stderr, "no select file: 3F0011001102\n");
        return GCNS_READ_PERSONAL_DATA;
    }
    r = sc_read_binary(card, 0, buffer, 0x180, 0);
    if (r < 0) {
        fprintf(stderr, "no read binary: %d\n", r);
        return GCNS_READ_PERSONAL_DATA;
    }

    return r;
}
