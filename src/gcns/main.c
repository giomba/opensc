#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#ifdef ENABLE_READLINE
#include <readline/history.h>
#include <readline/readline.h>
#endif
#if !defined(_WIN32)
#include <arpa/inet.h> /* for htons() */
#endif

#include <getopt.h>

#include "common/compat_strlcpy.h"
#include "gcns.h"
#include "libopensc/asn1.h"
#include "libopensc/cardctl.h"
#include "libopensc/cards.h"
#include "libopensc/internal.h"
#include "libopensc/iso7816.h"
#include "libopensc/log.h"
#include "libopensc/opensc.h"
#include "tools/util.h"

int main(int argc, char *argv[]) {
    int r;

    printf("OpenSC version: %s\n", sc_get_version());

    r = gcns_init();
    if (r != GCNS_SUCCESS) {
        fprintf(stderr, "Init Error\n");
        return GCNS_INIT;
    }

    u8 buffer[2048];
    r = gcns_read_personal_data(buffer, 2048);

    if (r < 0) {
        fprintf(stderr, "Read personal data error\n");
        return GCNS_READ_PERSONAL_DATA;
    }

    util_hex_dump_asc(stdout, buffer, r, 0);

    r = gcns_close();
    if (r != GCNS_SUCCESS) {
        return GCNS_CLOSE;
    }

    return GCNS_SUCCESS;
}
