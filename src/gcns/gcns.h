#ifndef GCNS_H
#define GCNS_H

#define GCNS_SUCCESS 0
#define GCNS_INIT -1001
#define GCNS_READ_PERSONAL_DATA -1002
#define GCNS_CLOSE -1003

int gcns_init();
int gcns_read_personal_data(u8 *buffer, size_t len);
int gcns_close();

#endif
