#ifndef GCNS_CPP
#define GCNS_CPP

#include <string>

namespace gcns {

enum Gender { GENDER_MALE, GENDER_FEMALE };

struct Date {
    uint16_t year;
    uint8_t month;
    uint8_t day;
};

class PersonalData {
   private:
    std::string first_name;
    std::string family_name;
    std::string fiscal_code;
    std::string birth_place;
    Date birth_date;
    std::string residence_place;
    Gender gender;
    Date issue_date;
    Date expiration_date;

   public:
    PersonalData(const uint8_t* personal_data, size_t len);
};

}  // namespace gcns

#endif
